#include <alpm.h>
#include <alpm_list.h>
#include <iostream>

using namespace std;

int main (){
    char *dbpath = (char *)"/var/lib/pacman";
    char *rootdir = (char *)"/";
    char *arch = (char *)"auto";
    char *ptr = (char *)"core";
    char *pkgname = (char *)"linux";
    enum _alpm_errno_t err;
    
    int r = 2;
    int y = (1 << 0);
    
    r |= y;
    
    if(r && y){
        cout << y << endl;
        cout << r << endl;
    }
    
    alpm_handle_t *handle = alpm_initialize(rootdir,dbpath,&err);
    
    if(!handle){
        cout << "Failed to initialize " << alpm_strerror(err) << endl;
    }else{
        cout << "Initialized." << endl;
    }

    cout << "Hello World" << endl;
    
    alpm_option_set_arch(handle,arch);
    
    alpm_db_t *db = alpm_register_syncdb(handle, ptr, ALPM_SIG_USE_DEFAULT);
    alpm_db_t *local = alpm_get_localdb(handle);
    
    for(const alpm_list_t *i = alpm_get_syncdbs(handle); i; i=alpm_list_next(i)){
        cout << alpm_db_get_name((alpm_db_t*)i->data) << endl;
        cout << alpm_db_get_valid((alpm_db_t*)i->data) << endl;
        
        int result = alpm_db_update(0, db);

        if(result < 0) {
            printf("Unable to update database: %s\n", alpm_strerror(alpm_errno(handle)));
        } else if(result == 1) {
            printf("Database already up to date\n");
        } else {
            printf("Database updated\n");
        }
        alpm_pkg_t *pkg = alpm_db_get_pkg((alpm_db_t*)i->data, pkgname);
        if (pkg != NULL){
            cout << alpm_pkg_get_version(pkg) << endl;
        }
    }
    
    cout << alpm_option_get_root(handle) << endl;
    cout << alpm_option_get_lockfile(handle) << endl;
    cout << alpm_option_get_dbpath(handle) << endl;
    cout << alpm_db_get_name(local) << endl;
    cout << alpm_db_get_valid(local) << endl;
    
    alpm_list_t *local_cache;
    local_cache = alpm_db_get_pkgcache(local);
    
    alpm_list_t *core_cache;
    core_cache = alpm_db_get_pkgcache(db);
    
    if(local_cache == NULL){
        cout << "Get something new" << endl;
    }
    
    for(alpm_list_t *i = local_cache; i; i = alpm_list_next(i)){
        alpm_pkg_t *pkg = (alpm_pkg_t*) i->data;
        cout << alpm_pkg_get_name(pkg) << ":" << alpm_pkg_get_version(pkg) << endl;
    }
    
    //cout << alpm__db_get_pkgcache(local)
    //cout << alpm_db_get_name(db) << endl;
    
    alpm_release(handle);
    return 0;
}


